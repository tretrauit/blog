+++
author = "TreTrauIT"
title = "Hello World!"
date = "2021-05-10"
description = "Hello World!"
tags = [
    "markdown",
    "css",
    "html",
    "themes",
]
categories = [
    "themes",
    "syntax",
]
series = ["Themes Guide"]
aliases = ["migrate-from-jekyl"]
image = "pawel-czerwinski-8uZPynIu-rQ-unsplash.jpg"
+++
So I created my first blog in my Hugo site, thats it!

## My first impression
In my opinion, [Hugo](https://gohugo.io/) is a very good framework for blogging, along with its fast speed and stablity. Configuring Hugo can be hard if you're a beginner, but [GitLab Template](https://gitlab.com/pages/hugo) has already done almost all work for you.

Hugo has a large number of themes too, so its very customizable. The theme I'm currently using is [Slack](https://github.com/CaiJimmy/hugo-theme-stack) - a simple card-style Hugo theme designed for bloggers.
